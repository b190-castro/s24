//console.log("Hello World");

let getCube = 2**3;
//console.log(getCube);

message = `The cube of 2 is ${getCube}`;
console.log(message);

let address = ["258 Washington Ave NW","California","90011"];
let [ street, state, zipCode ] = address;
console.log(`I live at ${street}, ${state}, ${zipCode}`);

let animal = {
	name: "Lolong",
	specie: "saltwater crocodile",
	weight: 1075,
	measurementFeet: 20,
	measurementInches: 3,
}

let { name, specie, weight, measurementFeet, measurementInches } = animal;
console.log(`${name} was a ${specie}. He weighed at ${weight} kgs with a measurement of ${measurementFeet} ft ${measurementInches} in.`);

let numbers = [1, 2, 3, 4, 5];
numbers.forEach(function(number){
	console.log(number);
});

let add = (a, b, c, d , e) => a + b + c + d + e;
let sumNumbers = add(1, 2, 3, 4, 5);
console.log(sumNumbers);

class Dog{
	constructor (name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
};

let myDog = new Dog
myDog.name = "Frankie";
myDog.age = 5;
myDog.breed = "Miniature Daschund"

console.log(myDog);
