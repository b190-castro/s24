console.log("Hello World");

// SECTION - Exponent operator --------------------------------



let firstNum = Math.pow(8,2);
console.log(firstNum);

/*
	let/const variableName = number ** exponent
*/

// ES6
let secondNum = 8**2;
console.log(secondNum);

// SECTION - Template Literals ---------------------------------
/*
	- allows to write strings without using the concatination operator (+);
	- helps greatly in terms of code readability
*/
let name = "John";

// pre-ES6
let message = " Hello " + name + "! Welcome to programming!"

//message without literals: message
console.log("Message without template literals: " + message)

// ES6
// single-line
message = `Hello ${name}! Welcome to programming!`;
console.log(`Message with template literals: ${message}`);

// multiple-line
const anotherMessage = `
${name} won the math competition.
He won it by solving the problem 8**2 with the solution of ${8**2}.
`;
// He won it by solving the problem 8**2 with the solution of ${secondNum}.
console.log(anotherMessage);
/*
	Miniactivity
		create 2 variables
			interestrate which has the value of .15;
			principal which has the value of 1000

			log in the console the total interest of the account that has the principal as its balance and the interest rate has its interest %

			send the output in the google chat
*/
/*
	- Template literals allow us to write strings with embedded JS expressions
	- expressions in general are any valid unit of code that resolves into a value
	- "${ }" are used to include JS expressions in strings using template literals
*/
// first solution (third variable)
let principal = 1000
let interestRate = .15
//let totalInterest = principal*interestRate;

//console.log(`Total earned interest: ${totalInterest}`);

// second solution (mathematical operation isnide the template literals)
console.log(`Total earned interest: ${principal*interestRate}`);

// SECTION - Array Destructuring ---------------------------------------------
/*
	- allows us to unpack elements in arrays to distinct variables
	- allows us to name array elements with variables instead of using index numbers
	- helps with code readability

	SYNTAX:
		let/const[ variableA, variableB, variableC ] = arrayName

	NOTE: we have to be mindful of the variableName and make sure it describes the right element that is stored inside it.
*/
const fullName = ["Juan","Dela","Cruz"];
// pre-array destructuring
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

// array destrucuring
const [ firstName, middleName, lastName ] = fullName;
console.log(firstName);
console.log(middleName);
console.log(lastName);

// using Template literals
console.log(`Hello, ${firstName} ${middleName} ${lastName}!`);

/*
	Miniactivity
		create a "person" onject with the following properties
			firstName = Jane
			middleName = Dela
			lastName = Cruz
		log in te console each of the properties as well as a greeting message for the person.
		send the otutput in the google chat
*/

// SECTION - Object Destructuring ---------------------------------------
/*
	- allows us to unpack properties of objects into distinct variables
	- shortens the syntax for accessing the properties from an object

	SYNTAX:
		let/const { propertyNAmeA, propertyNameB, propertyNameC } = objectName;
*/

/*let person = ["Jane", "Dela", "Cruz"]
let [ firstNames, middleNames, lastNames ] = person;
console.log(firstNames);
console.log(middleNames);
console.log(lastNames);
console.log (`Hello, ${firstNames} ${middleNames} ${lastNames}!`);*/

// object
const person = {
	givenName: "Jane", // firstName - error will be returned since there is already a similar variable declared
	maidenName: "Dela",// middleName - error will be returned since there is already a similar variable declared
	familyName: "Cruz"// lastName - error will be returned since there is already a similar variable declared
}
//pre-es6/pre-object destructure

console.log(person.givenName);
console.log(person.maidenName);
console.log(person.familyName);
console.log(`Hello, ${person.givenName} ${person.maidenName} ${person.familyName}!`);

// object desctructuring
const { givenName, maidenName, familyName } = person;
console.log(givenName);
console.log(maidenName);
console.log(familyName);
console.log(`Hello, ${givenName} ${maidenName} ${familyName}!`);

// using object destructuring as a parameter of a function
function getFullName({givenName, maidenName, familyName}){
	console.log(`${givenName} ${maidenName} ${familyName}`);
};
getFullName(person);
/*
	Miniactivity
		create a pet object
			name
			trick
			treat
		create a function that will receive a object destructuring as its parameter
*/

const pet = {
	namePet: "Baldo",
	trick: "Sleep",
	treat: "Crispy Pata"
}

function getPet({namePet, trick, treat}){
	console.log(`${namePet}, ${trick}`);
	console.log(`good ${trick}`);
	console.log(`here is ${treat} for you`);
};
getPet(pet);

// SECTION - Arrow Function -------------------------------------------

/*
	- compact alternative syntax to traditional functions
	- useful for code snippets where creating functions will not be reused in any other portion of the 
*/

/*const hello = ()=>{
	console.log("Hello World");
}*/
// pre-eES6/arrow function
/*function printFullName (firstName, middleInitial, lastName){
	console.log(`${firstName} ${middleInitial} ${lastName}`);
};*/

// arrow function in ES6
/*
	let/const variableName = (paramterA, parameterB, parameterC) => {
	console.log(); /statement/expressions
	}
*/
let printFullName = (firstName, middleInitial, lastName) => {
	console.log(`${firstName} ${middleInitial} ${lastName}`);
}
printFullName("Portgas","D.","Ace");
printFullName("Sakura","H.","Uchiha");

// arrow function with loops
// pre-arrow function
const students = ["John","Jane","Judy"];
students.forEach(function(student){
	console.log(`${student} is a student.`);
});

// SECTION - implicit return statement
/*
	- There are instances where we can omit the "return" statement;
	- This wroks because even without the "return" statement, JS can implicitly
*/
/*
	Miniactivity
		create an arrow function with 'add' as its name
			2 parameters, x y
			statement: using return keyword, add the x and y
		log in the console the function value once it is called with proper arguments
*/
// pre-arrow function
/*let add = (x,y) => {
	return x + y;
}
console.log(add(2,3));*/

// arrow function
const add = (x,y) => x+y;
let addVariable = add (99,888);
console.log(addVariable);


// SECTION - Default Argument Value
/*
	the name = "User" sets the default value for the function greet() once it is called without parameters
*/
const greet = (name = "User") =>{
	return `Good Morning, ${name}!`
}

console.log(greet("John"));

// this would return "Good Moring, User" because the function is called without any arguments
//console.log(greet());

// SECTION - Class-based Object BluePrints
/*
	- Allows creation/instantiation of objects using classes as blueprints
*/
/*
	Creating a class
	- "constructor" is a special method of a class for creating/initializing an object for that class
	- "this" keyword refers to properties of an object created from the class; this allows us to reassign values for the properties inside the class

	SYNTAX:
		class className{
			constructor (objectPropertyA, objectPropertyB){
			this.objectPropertyA = objectPropertyA;
			this.objectPropertyB = objectPropertyB;
			}
		}
*/
class Car{
	constructor (brand, name, year){
		this.brand = brand;
		this.name = name;
		this.year = year;
	}
};

// first instance
// using initializer and/or dot notation, create a car object using the Car class that we have created.
/*
	- "new" creates a new object with the given arguements as the values of its properties
	- no arguements provided will create an object without any values assigned to it, meaning the properties would return "undefined"

	SYNTAX:
		let/const variableName = new ClassName();
*/
/*
	- creating with const keyword and assigning it a value of an object makes it so we cannot re-assign another data type i.e. array
	- it does not mean that it's properties cannot be changed/manipulated
*/

const myCar = new Car();
//console.log(myCar);

// Values for each property under the Car class
myCar.brand = "Ford";
myCar.name = "Ranger Raptor";
myCar.year = 2021;

console.log(myCar);


// second instance - using arguements
const myNewCar = new Car("Toyota", "Vios", 2021);
console.log(myNewCar);